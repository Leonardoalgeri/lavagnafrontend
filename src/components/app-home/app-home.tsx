import { Component, h } from '@stencil/core';
import {getData, Post } from "../function/function";
//import { DataController } from "../datacontroller/datacontroller";


// import { FormGroup, Icon, Popover } from '@blueprintjs/core';

@Component({
  tag: 'app-home',
  styleUrls: [
    'app-home.css'
  ]
})

export class AppHome {
  componentDidLoad() {
    document.getElementById("messaggio").setAttribute("rows","5");
    getData();
  }

  reset(): void {
    var Form: HTMLFormElement = document.getElementById("create-course-form") as HTMLFormElement;
    Form.reset();
    document.getElementsByTagName("data-panel")[0].hidden = true;
  }

  contacts(): void {
    if (!window.navigator.onLine) {
      const alert = document.createElement('ion-alert');
      alert.header = "Sei offline!";
      alert.message = 'È necessario essere connessi a internet per visualizzare questa sezione 😊';
      alert.buttons = ['OK'];
      document.body.appendChild(alert);
      alert.present();
    }
    else {
      if (window.matchMedia('(display-mode: standalone)').matches) {
        location.replace("https://alge97.netlify.com/")
      }
      else {
        window.open("https://alge97.netlify.com/", "_blank");
      }
    }
  }

  getData(): void{
    getData();
  }

  calculate(): void {
    var Area: HTMLTextAreaElement=document.getElementById("messaggio") as HTMLTextAreaElement;
    if(Area.value!=""){
      Post(Area.value);
      Area.value="";
    }
    else{
      const alert = document.createElement('ion-alert');
      alert.header = "Errore!";
      alert.message = 'Impossibile inviare un messaggio vuoto, di qualcosa 😊';
      alert.buttons = ['OK'];
      document.body.appendChild(alert);
      alert.present();
    }
    setTimeout(function(){ getData(); }, 1000);
    setTimeout(function(){ getData(); }, 3000);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>Public board</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class="ion-padding">

        <div id="item1">
          <textarea
            readonly
            value=""
            id="messages">
          </textarea>
        </div>

        <div id="item2">
        <form>
        <textarea placeholder="Inserisci il tuo messaggio qui!" id="messaggio"></textarea>
        <ion-button expand="block" fill="outline" shape="round" onClick={this.calculate}>Invia messaggio!</ion-button>
        <ion-button expand="block" color="medium" fill="outline" shape="round" onClick={this.getData}>Controlla nuovi messaggi</ion-button>
        </form>
        </div>
      </ion-content>,

      <ion-footer>
        <ion-toolbar color="medium" onClick={this.contacts}>
          <ion-title text-center>Creato da Alge97</ion-title>
          <ion-title text-right>V1.5</ion-title>
        </ion-toolbar>
      </ion-footer>

    ];
  }
}
