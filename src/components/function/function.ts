const url: string = "https://lavagna.herokuapp.com/"
export function controlloSubnet(subnet: string): string {
    console.log("La subnet è: " + subnet);

    var primo: string = subnet.split('.')[0];
    var secondo: string = subnet.split('.')[1];
    var terzo: string = subnet.split('.')[2];
    var quarto: string = subnet.split('.')[3];
    //var err = subnet.split('.')[4];

    // //Primo controllo se la subnet è valida (cioè nella forma n.n.n.n e che non contenga lettere)
    // if (quarto == undefined || err != undefined || primo < 0) {
    //     return false;  //retunrn -1 con messaggio d'errore
    // }

    //Conversione dei "pezzi" di numero in binario
    var primobin = parseInt(primo).toString(2);
    var secondobin = parseInt(secondo).toString(2);
    var terzobin = parseInt(terzo).toString(2);
    var quartobin = parseInt(quarto).toString(2);
    var numero = primobin + secondobin + terzobin + quartobin;

    //Controllo che i singoli blocchi non superino 8 bit
    if (primobin.length > 8 || secondobin.length > 8 || terzobin.length > 8 || quartobin.length > 8)
        return null;

    //Controllo che ci sia almeno un host
    if (quartobin[7] == "1" && quartobin[7] != undefined)
        return null;

    //Ricerca di ulteriori errori nel numero binario della subnet
    var i;
    var wrong = false;
    for (i = 0; i < numero.length; i++) {
        if (numero[i] == "0")
            wrong = true;
        else if (wrong == true)
            return null;
    }

    return numero;
}

export function subnetslesc(num)  //Calcolo della subnet in formato /num
{
    //alert(num);
    var sub = 0;
    for (var i = 0; i < num.length; i++) {
        if (num[i] == 1)
            sub++;
    }
    //document.getElementById("subnetmask").innerHTML ="La <b>subnet</b> in formato / &egrave <b>/"+sub+"</b>";
    return sub;
}

export async function getData() {
    const response = await fetch(url+"all");
    const json = await response.json();
    var Area: HTMLTextAreaElement = document.getElementById("messages") as HTMLTextAreaElement;
    Area.value = "";
    for (let i in json) {
        Area.value = Area.value + json[i].messaggio + "\n";

    }

}

export function Post(message) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    
    xhr.addEventListener("readystatechange", function() {
      if(this.readyState === 4) {
        console.log(this.responseText);
      }
    });
    
    xhr.open("POST", url+"add?messaggio="+message);
    
    xhr.send();
}
