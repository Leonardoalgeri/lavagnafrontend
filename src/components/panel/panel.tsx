import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'data-panel',
  styleUrl: 'panel.css',
  shadow: true
})
export class Panel {
  @Prop() host: string;
  @Prop() magicnumber: string;
  @Prop() potenza: string;
  render() {
    return (
      <Host>
        <p>Il numero di <b>Host disponibili</b> con questa subnet sono: <b>{this.host}</b></p>
        <p>Il <b>magicnumber</b> è: <b>{this.magicnumber}</b> (2<sup>{this.potenza}</sup>)</p>
      </Host>
    );
  }

}
